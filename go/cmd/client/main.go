package main

import (
	"context"
	"fmt"
	"time"

	pb "gitlab.com/usvc/playground/grpc-example/go/internal/helloworld"
	"google.golang.org/grpc"
)

func main() {
	connection, err := grpc.Dial("localhost:12345", grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		panic(err)
	}
	defer connection.Close()
	c := pb.NewHelloWorldClient(connection)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	res, err := c.SayHello(ctx, &pb.HelloRequest{
		Name: "world",
	})
	if err != nil {
		panic(err)
	}
	fmt.Println(res.GetReply())
}
