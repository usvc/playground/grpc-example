package main

import (
	"context"

	pb "gitlab.com/usvc/playground/grpc-example/go/internal/helloworld"
	// "google.golang.org/grpc/status"
)

type helloWorldServer struct {
	pb.UnimplementedHelloWorldServer
}

func (*helloWorldServer) SayHello(ctx context.Context, req *pb.HelloRequest) (*pb.HelloResponse, error) {
	return &pb.HelloResponse{
		Reply: "hello " + req.Name,
	}, nil
}
