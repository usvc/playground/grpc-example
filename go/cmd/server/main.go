package main

import (
	"net"

	pb "gitlab.com/usvc/playground/grpc-example/go/internal/helloworld"
	"google.golang.org/grpc"
)

func main() {
	listener, err := net.Listen("tcp", "0.0.0.0:12345")
	if err != nil {
		panic(err)
	}
	grpcServer := grpc.NewServer()
	pb.RegisterHelloWorldServer(grpcServer, &helloWorldServer{})
	if err := grpcServer.Serve(listener); err != nil {
		panic(err)
	}
}
